// requires...
const fs = require('fs');
const path = require('path');

// constants...
const extensions = ['log', 'txt', 'json', 'yaml', 'xml', 'js'];

function createFile (req, res) {
  const fileName = req.body.filename;
  const fileContent = req.body.content;

  if (!fileContent) {
    res.status(400).send({ "message": "Please specify 'content' parameter" });
  } else if (!fileName) {
    res.status(400).send({ "message": "Please specify 'filename' parameter" });
  } else if (!extensions.includes(fileName.split('.').slice(-1)[0])) {
    res.status(400).send({ "message": "Allowed file extension are log, txt, json, yaml, xml, js" });
  } 
  else {
    fs.writeFile(`./files/${fileName}`, fileContent, function (err) {
      if (err) {
        console.log(err);
      }
    });
  
    res.status(200).send({ "message": "File created successfully" });
  }
}

function getFiles (_, res) {
  fs.readdir('./files/', (err, files) => {
    if (err) {
      res.status(400).send({
        "message": "Client error"});
    }
    
    res.status(200).send({
      "message": "Success",
      "files": files});
  });  
}

const getFile = (req, res) => {
  if (fs.existsSync(`./files${req.url}`)) {
    const fileName = req.url;
    const fileExtension = path.extname(fileName).slice(1);
    const fileContent = fs.readFileSync(`./files${fileName}`).toString();
    const fileDate = fs.statSync(`./files/${fileName}`).birthtime;

    res.status(200).send({
      "message": "Success",
      "filename": fileName.slice(1),
      "content": fileContent,
      "extension": fileExtension,
      "uploadedDate": fileDate});
  } else {
    const message = `No file with '${req.url.slice(1)}' filename found`
    res.status(400).send({"message": message});
  }
};

function editFIle(req, res) {
  const fileName = req.body.filename;
  const fileNewContent = req.body.newContent;

  if (!fileNewContent) {
    res.status(400).send({"message": "Please specify 'newContent' parameter"})
  } else if (fs.existsSync(`./files/${fileName}`)) {
    fs.writeFile(`./files/${fileName}`, fileNewContent, (err) => {
      if (err) {
        console.log(err);
      }
    });

    const message = `File '${fileName}' updated succesfully`
  
    res.status(200).send({"message": message})
  } else {
    const message = `File '${fileName}' does not exist`
    res.status(400).send({"message": message})
  }
}

function deleteFile(req, res) {
  const fileName = req.url;

  if (fs.existsSync(`./files${fileName}`)) {
    fs.unlink(`./files${fileName}`, (err) => {
      if (err) {
        console.log(err);
      }
    });
  
    const message = `File '${fileName.slice(1)}' deleted succesfully`;
    res.status(200).send({"message": message});
  } else {
    const message = `File '${fileName.slice(1)}' does not exist`
    res.status(400).send({"message": message});
  }
  
  
}

module.exports = {
  createFile,
  getFiles,
  getFile,
  editFIle,
  deleteFile
};